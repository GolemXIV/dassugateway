import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { DassuGatewayTestModule } from '../../../../test.module';
import { FileCategoryFieldComponent } from 'app/entities/DassuCore/file-category-field/file-category-field.component';
import { FileCategoryFieldService } from 'app/entities/DassuCore/file-category-field/file-category-field.service';
import { FileCategoryField } from 'app/shared/model/DassuCore/file-category-field.model';

describe('Component Tests', () => {
  describe('FileCategoryField Management Component', () => {
    let comp: FileCategoryFieldComponent;
    let fixture: ComponentFixture<FileCategoryFieldComponent>;
    let service: FileCategoryFieldService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [DassuGatewayTestModule],
        declarations: [FileCategoryFieldComponent],
      })
        .overrideTemplate(FileCategoryFieldComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FileCategoryFieldComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FileCategoryFieldService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new FileCategoryField(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.fileCategoryFields && comp.fileCategoryFields[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});

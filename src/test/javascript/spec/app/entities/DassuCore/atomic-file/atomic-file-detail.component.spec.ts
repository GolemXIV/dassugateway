import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { DassuGatewayTestModule } from '../../../../test.module';
import { AtomicFileDetailComponent } from 'app/entities/DassuCore/atomic-file/atomic-file-detail.component';
import { AtomicFile } from 'app/shared/model/DassuCore/atomic-file.model';

describe('Component Tests', () => {
  describe('AtomicFile Management Detail Component', () => {
    let comp: AtomicFileDetailComponent;
    let fixture: ComponentFixture<AtomicFileDetailComponent>;
    const route = ({ data: of({ atomicFile: new AtomicFile(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [DassuGatewayTestModule],
        declarations: [AtomicFileDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(AtomicFileDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(AtomicFileDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load atomicFile on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.atomicFile).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});

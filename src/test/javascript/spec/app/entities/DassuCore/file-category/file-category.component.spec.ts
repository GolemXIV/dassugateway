import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { DassuGatewayTestModule } from '../../../../test.module';
import { FileCategoryComponent } from 'app/entities/DassuCore/file-category/file-category.component';
import { FileCategoryService } from 'app/entities/DassuCore/file-category/file-category.service';
import { FileCategory } from 'app/shared/model/DassuCore/file-category.model';

describe('Component Tests', () => {
  describe('FileCategory Management Component', () => {
    let comp: FileCategoryComponent;
    let fixture: ComponentFixture<FileCategoryComponent>;
    let service: FileCategoryService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [DassuGatewayTestModule],
        declarations: [FileCategoryComponent],
      })
        .overrideTemplate(FileCategoryComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FileCategoryComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FileCategoryService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new FileCategory(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.fileCategories && comp.fileCategories[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});

import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { DassuGatewayTestModule } from '../../../../test.module';
import { MetricTypeUpdateComponent } from 'app/entities/DassuCore/metric-type/metric-type-update.component';
import { MetricTypeService } from 'app/entities/DassuCore/metric-type/metric-type.service';
import { MetricType } from 'app/shared/model/DassuCore/metric-type.model';

describe('Component Tests', () => {
  describe('MetricType Management Update Component', () => {
    let comp: MetricTypeUpdateComponent;
    let fixture: ComponentFixture<MetricTypeUpdateComponent>;
    let service: MetricTypeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [DassuGatewayTestModule],
        declarations: [MetricTypeUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(MetricTypeUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MetricTypeUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MetricTypeService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new MetricType(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new MetricType();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});

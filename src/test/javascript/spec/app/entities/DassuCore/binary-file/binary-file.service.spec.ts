import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { BinaryFileService } from 'app/entities/DassuCore/binary-file/binary-file.service';
import { IBinaryFile, BinaryFile } from 'app/shared/model/DassuCore/binary-file.model';
import { StorageType } from 'app/shared/model/enumerations/storage-type.model';

describe('Service Tests', () => {
  describe('BinaryFile Service', () => {
    let injector: TestBed;
    let service: BinaryFileService;
    let httpMock: HttpTestingController;
    let elemDefault: IBinaryFile;
    let expectedResult: IBinaryFile | IBinaryFile[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(BinaryFileService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new BinaryFile(0, 'AAAAAAA', 0, 'AAAAAAA', 'AAAAAAA', StorageType.LOCAL);
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a BinaryFile', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new BinaryFile()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a BinaryFile', () => {
        const returnedFromService = Object.assign(
          {
            name: 'BBBBBB',
            version: 1,
            storageFilePath: 'BBBBBB',
            md5: 'BBBBBB',
            storageType: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of BinaryFile', () => {
        const returnedFromService = Object.assign(
          {
            name: 'BBBBBB',
            version: 1,
            storageFilePath: 'BBBBBB',
            md5: 'BBBBBB',
            storageType: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a BinaryFile', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});

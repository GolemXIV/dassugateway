import { element, by, ElementFinder } from 'protractor';

export class BinaryFileComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-binary-file div table .btn-danger'));
  title = element.all(by.css('jhi-binary-file div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class BinaryFileUpdatePage {
  pageTitle = element(by.id('jhi-binary-file-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  nameInput = element(by.id('field_name'));
  versionInput = element(by.id('field_version'));
  storageFilePathInput = element(by.id('field_storageFilePath'));
  md5Input = element(by.id('field_md5'));
  storageTypeSelect = element(by.id('field_storageType'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setNameInput(name: string): Promise<void> {
    await this.nameInput.sendKeys(name);
  }

  async getNameInput(): Promise<string> {
    return await this.nameInput.getAttribute('value');
  }

  async setVersionInput(version: string): Promise<void> {
    await this.versionInput.sendKeys(version);
  }

  async getVersionInput(): Promise<string> {
    return await this.versionInput.getAttribute('value');
  }

  async setStorageFilePathInput(storageFilePath: string): Promise<void> {
    await this.storageFilePathInput.sendKeys(storageFilePath);
  }

  async getStorageFilePathInput(): Promise<string> {
    return await this.storageFilePathInput.getAttribute('value');
  }

  async setMd5Input(md5: string): Promise<void> {
    await this.md5Input.sendKeys(md5);
  }

  async getMd5Input(): Promise<string> {
    return await this.md5Input.getAttribute('value');
  }

  async setStorageTypeSelect(storageType: string): Promise<void> {
    await this.storageTypeSelect.sendKeys(storageType);
  }

  async getStorageTypeSelect(): Promise<string> {
    return await this.storageTypeSelect.element(by.css('option:checked')).getText();
  }

  async storageTypeSelectLastOption(): Promise<void> {
    await this.storageTypeSelect.all(by.tagName('option')).last().click();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class BinaryFileDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-binaryFile-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-binaryFile'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}

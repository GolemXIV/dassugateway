import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../../page-objects/jhi-page-objects';

import { CompositFileComponentsPage, CompositFileDeleteDialog, CompositFileUpdatePage } from './composit-file.page-object';

const expect = chai.expect;

describe('CompositFile e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let compositFileComponentsPage: CompositFileComponentsPage;
  let compositFileUpdatePage: CompositFileUpdatePage;
  let compositFileDeleteDialog: CompositFileDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load CompositFiles', async () => {
    await navBarPage.goToEntity('composit-file');
    compositFileComponentsPage = new CompositFileComponentsPage();
    await browser.wait(ec.visibilityOf(compositFileComponentsPage.title), 5000);
    expect(await compositFileComponentsPage.getTitle()).to.eq('dassuGatewayApp.dassuCoreCompositFile.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(compositFileComponentsPage.entities), ec.visibilityOf(compositFileComponentsPage.noResult)),
      1000
    );
  });

  it('should load create CompositFile page', async () => {
    await compositFileComponentsPage.clickOnCreateButton();
    compositFileUpdatePage = new CompositFileUpdatePage();
    expect(await compositFileUpdatePage.getPageTitle()).to.eq('dassuGatewayApp.dassuCoreCompositFile.home.createOrEditLabel');
    await compositFileUpdatePage.cancel();
  });

  it('should create and save CompositFiles', async () => {
    const nbButtonsBeforeCreate = await compositFileComponentsPage.countDeleteButtons();

    await compositFileComponentsPage.clickOnCreateButton();

    await promise.all([
      compositFileUpdatePage.statusSelectLastOption(),
      compositFileUpdatePage.binaryFileSelectLastOption(),
      compositFileUpdatePage.fileCategorySelectLastOption(),
    ]);

    await compositFileUpdatePage.save();
    expect(await compositFileUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await compositFileComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last CompositFile', async () => {
    const nbButtonsBeforeDelete = await compositFileComponentsPage.countDeleteButtons();
    await compositFileComponentsPage.clickOnLastDeleteButton();

    compositFileDeleteDialog = new CompositFileDeleteDialog();
    expect(await compositFileDeleteDialog.getDialogTitle()).to.eq('dassuGatewayApp.dassuCoreCompositFile.delete.question');
    await compositFileDeleteDialog.clickOnConfirmButton();

    expect(await compositFileComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});

import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../../page-objects/jhi-page-objects';

import {
  FileCategoryFieldValueComponentsPage,
  FileCategoryFieldValueDeleteDialog,
  FileCategoryFieldValueUpdatePage,
} from './file-category-field-value.page-object';

const expect = chai.expect;

describe('FileCategoryFieldValue e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let fileCategoryFieldValueComponentsPage: FileCategoryFieldValueComponentsPage;
  let fileCategoryFieldValueUpdatePage: FileCategoryFieldValueUpdatePage;
  let fileCategoryFieldValueDeleteDialog: FileCategoryFieldValueDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load FileCategoryFieldValues', async () => {
    await navBarPage.goToEntity('file-category-field-value');
    fileCategoryFieldValueComponentsPage = new FileCategoryFieldValueComponentsPage();
    await browser.wait(ec.visibilityOf(fileCategoryFieldValueComponentsPage.title), 5000);
    expect(await fileCategoryFieldValueComponentsPage.getTitle()).to.eq('dassuGatewayApp.dassuCoreFileCategoryFieldValue.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(fileCategoryFieldValueComponentsPage.entities), ec.visibilityOf(fileCategoryFieldValueComponentsPage.noResult)),
      1000
    );
  });

  it('should load create FileCategoryFieldValue page', async () => {
    await fileCategoryFieldValueComponentsPage.clickOnCreateButton();
    fileCategoryFieldValueUpdatePage = new FileCategoryFieldValueUpdatePage();
    expect(await fileCategoryFieldValueUpdatePage.getPageTitle()).to.eq(
      'dassuGatewayApp.dassuCoreFileCategoryFieldValue.home.createOrEditLabel'
    );
    await fileCategoryFieldValueUpdatePage.cancel();
  });

  it('should create and save FileCategoryFieldValues', async () => {
    const nbButtonsBeforeCreate = await fileCategoryFieldValueComponentsPage.countDeleteButtons();

    await fileCategoryFieldValueComponentsPage.clickOnCreateButton();

    await promise.all([
      fileCategoryFieldValueUpdatePage.setValueInput('value'),
      fileCategoryFieldValueUpdatePage.compositFileSelectLastOption(),
      fileCategoryFieldValueUpdatePage.fileCategoryFieldSelectLastOption(),
      fileCategoryFieldValueUpdatePage.metricSelectLastOption(),
    ]);

    expect(await fileCategoryFieldValueUpdatePage.getValueInput()).to.eq('value', 'Expected Value value to be equals to value');

    await fileCategoryFieldValueUpdatePage.save();
    expect(await fileCategoryFieldValueUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await fileCategoryFieldValueComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last FileCategoryFieldValue', async () => {
    const nbButtonsBeforeDelete = await fileCategoryFieldValueComponentsPage.countDeleteButtons();
    await fileCategoryFieldValueComponentsPage.clickOnLastDeleteButton();

    fileCategoryFieldValueDeleteDialog = new FileCategoryFieldValueDeleteDialog();
    expect(await fileCategoryFieldValueDeleteDialog.getDialogTitle()).to.eq(
      'dassuGatewayApp.dassuCoreFileCategoryFieldValue.delete.question'
    );
    await fileCategoryFieldValueDeleteDialog.clickOnConfirmButton();

    expect(await fileCategoryFieldValueComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});

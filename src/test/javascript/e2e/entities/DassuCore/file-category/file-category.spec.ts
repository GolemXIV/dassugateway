import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../../page-objects/jhi-page-objects';

import { FileCategoryComponentsPage, FileCategoryDeleteDialog, FileCategoryUpdatePage } from './file-category.page-object';

const expect = chai.expect;

describe('FileCategory e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let fileCategoryComponentsPage: FileCategoryComponentsPage;
  let fileCategoryUpdatePage: FileCategoryUpdatePage;
  let fileCategoryDeleteDialog: FileCategoryDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load FileCategories', async () => {
    await navBarPage.goToEntity('file-category');
    fileCategoryComponentsPage = new FileCategoryComponentsPage();
    await browser.wait(ec.visibilityOf(fileCategoryComponentsPage.title), 5000);
    expect(await fileCategoryComponentsPage.getTitle()).to.eq('dassuGatewayApp.dassuCoreFileCategory.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(fileCategoryComponentsPage.entities), ec.visibilityOf(fileCategoryComponentsPage.noResult)),
      1000
    );
  });

  it('should load create FileCategory page', async () => {
    await fileCategoryComponentsPage.clickOnCreateButton();
    fileCategoryUpdatePage = new FileCategoryUpdatePage();
    expect(await fileCategoryUpdatePage.getPageTitle()).to.eq('dassuGatewayApp.dassuCoreFileCategory.home.createOrEditLabel');
    await fileCategoryUpdatePage.cancel();
  });

  it('should create and save FileCategories', async () => {
    const nbButtonsBeforeCreate = await fileCategoryComponentsPage.countDeleteButtons();

    await fileCategoryComponentsPage.clickOnCreateButton();

    await promise.all([
      fileCategoryUpdatePage.setNameInput('name'),
      fileCategoryUpdatePage.setDescriptionInput('description'),
      fileCategoryUpdatePage.metricSelectLastOption(),
    ]);

    expect(await fileCategoryUpdatePage.getNameInput()).to.eq('name', 'Expected Name value to be equals to name');
    expect(await fileCategoryUpdatePage.getDescriptionInput()).to.eq(
      'description',
      'Expected Description value to be equals to description'
    );

    await fileCategoryUpdatePage.save();
    expect(await fileCategoryUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await fileCategoryComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last FileCategory', async () => {
    const nbButtonsBeforeDelete = await fileCategoryComponentsPage.countDeleteButtons();
    await fileCategoryComponentsPage.clickOnLastDeleteButton();

    fileCategoryDeleteDialog = new FileCategoryDeleteDialog();
    expect(await fileCategoryDeleteDialog.getDialogTitle()).to.eq('dassuGatewayApp.dassuCoreFileCategory.delete.question');
    await fileCategoryDeleteDialog.clickOnConfirmButton();

    expect(await fileCategoryComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});

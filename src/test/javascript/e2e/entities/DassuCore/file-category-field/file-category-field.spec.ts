import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../../page-objects/jhi-page-objects';

import {
  FileCategoryFieldComponentsPage,
  FileCategoryFieldDeleteDialog,
  FileCategoryFieldUpdatePage,
} from './file-category-field.page-object';

const expect = chai.expect;

describe('FileCategoryField e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let fileCategoryFieldComponentsPage: FileCategoryFieldComponentsPage;
  let fileCategoryFieldUpdatePage: FileCategoryFieldUpdatePage;
  let fileCategoryFieldDeleteDialog: FileCategoryFieldDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load FileCategoryFields', async () => {
    await navBarPage.goToEntity('file-category-field');
    fileCategoryFieldComponentsPage = new FileCategoryFieldComponentsPage();
    await browser.wait(ec.visibilityOf(fileCategoryFieldComponentsPage.title), 5000);
    expect(await fileCategoryFieldComponentsPage.getTitle()).to.eq('dassuGatewayApp.dassuCoreFileCategoryField.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(fileCategoryFieldComponentsPage.entities), ec.visibilityOf(fileCategoryFieldComponentsPage.noResult)),
      1000
    );
  });

  it('should load create FileCategoryField page', async () => {
    await fileCategoryFieldComponentsPage.clickOnCreateButton();
    fileCategoryFieldUpdatePage = new FileCategoryFieldUpdatePage();
    expect(await fileCategoryFieldUpdatePage.getPageTitle()).to.eq('dassuGatewayApp.dassuCoreFileCategoryField.home.createOrEditLabel');
    await fileCategoryFieldUpdatePage.cancel();
  });

  it('should create and save FileCategoryFields', async () => {
    const nbButtonsBeforeCreate = await fileCategoryFieldComponentsPage.countDeleteButtons();

    await fileCategoryFieldComponentsPage.clickOnCreateButton();

    await promise.all([
      fileCategoryFieldUpdatePage.setNameInput('name'),
      fileCategoryFieldUpdatePage.setMetaInput('meta'),
      // fileCategoryFieldUpdatePage.fileCategorySelectLastOption(),
    ]);

    expect(await fileCategoryFieldUpdatePage.getNameInput()).to.eq('name', 'Expected Name value to be equals to name');
    expect(await fileCategoryFieldUpdatePage.getMetaInput()).to.eq('meta', 'Expected Meta value to be equals to meta');

    await fileCategoryFieldUpdatePage.save();
    expect(await fileCategoryFieldUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await fileCategoryFieldComponentsPage.countDeleteButtons()).to.eq(
      nbButtonsBeforeCreate + 1,
      'Expected one more entry in the table'
    );
  });

  it('should delete last FileCategoryField', async () => {
    const nbButtonsBeforeDelete = await fileCategoryFieldComponentsPage.countDeleteButtons();
    await fileCategoryFieldComponentsPage.clickOnLastDeleteButton();

    fileCategoryFieldDeleteDialog = new FileCategoryFieldDeleteDialog();
    expect(await fileCategoryFieldDeleteDialog.getDialogTitle()).to.eq('dassuGatewayApp.dassuCoreFileCategoryField.delete.question');
    await fileCategoryFieldDeleteDialog.clickOnConfirmButton();

    expect(await fileCategoryFieldComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});

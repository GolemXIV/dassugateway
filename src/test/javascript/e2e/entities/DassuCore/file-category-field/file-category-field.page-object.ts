import { element, by, ElementFinder } from 'protractor';

export class FileCategoryFieldComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-file-category-field div table .btn-danger'));
  title = element.all(by.css('jhi-file-category-field div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class FileCategoryFieldUpdatePage {
  pageTitle = element(by.id('jhi-file-category-field-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  nameInput = element(by.id('field_name'));
  metaInput = element(by.id('field_meta'));

  fileCategorySelect = element(by.id('field_fileCategory'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setNameInput(name: string): Promise<void> {
    await this.nameInput.sendKeys(name);
  }

  async getNameInput(): Promise<string> {
    return await this.nameInput.getAttribute('value');
  }

  async setMetaInput(meta: string): Promise<void> {
    await this.metaInput.sendKeys(meta);
  }

  async getMetaInput(): Promise<string> {
    return await this.metaInput.getAttribute('value');
  }

  async fileCategorySelectLastOption(): Promise<void> {
    await this.fileCategorySelect.all(by.tagName('option')).last().click();
  }

  async fileCategorySelectOption(option: string): Promise<void> {
    await this.fileCategorySelect.sendKeys(option);
  }

  getFileCategorySelect(): ElementFinder {
    return this.fileCategorySelect;
  }

  async getFileCategorySelectedOption(): Promise<string> {
    return await this.fileCategorySelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class FileCategoryFieldDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-fileCategoryField-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-fileCategoryField'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}

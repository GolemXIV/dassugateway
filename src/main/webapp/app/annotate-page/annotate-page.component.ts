import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import Konva from 'konva';
import Shape = Konva.Shape;

@Component({
  selector: 'jhi-annotate-page',
  templateUrl: './annotate-page.component.html',
  styleUrls: ['./annotate-page.component.scss'],
})
export class AnnotatePageComponent implements OnInit {
  @ViewChild('canvas', { static: true })
  canvas: ElementRef<HTMLDivElement>;
  currentPage = 1;

  private ctx: Konva.Stage | null = null;

  public data = [require('./page1.json'), require('./page2.json'), require('./page3.json')];

  // SHIT delete
  private imageSrc = [require('./page1.png'), require('./page2.png'), require('./page3.png')];

  constructor(private router: Router, private activatedRoute: ActivatedRoute) {
    // router.events.subscribe((url:any) => console.log(url));
    // console.log('~~~');
    // console.log(this.activatedRoute.snapshot.url);
  }

  ngOnInit(): void {
    // this.ctx = this.canvas.nativeElement.getContext('2d');
    this.drawPage(1);
  }

  drawPage(page: number): void {
    if (this.ctx) {
      this.ctx.clear();
    }

    this.ctx = new Konva.Stage({
      container: this.canvas.nativeElement,
      width: this.data[this.currentPage - 1].width,
      height: this.data[this.currentPage - 1].height,
      draggable: true,
    });

    // add canvas element
    const layer = new Konva.Layer();
    this.ctx.add(layer);

    const group = new Konva.Group();
    // alternative API:
    // main API:
    this.drawAreas(layer, group, page - 1);
    this.drawImage(layer, group, this.imageSrc[page - 1]);
  }

  drawImage(layer: Konva.Layer, group: Konva.Group, imageUrl: string): void {
    Konva.Image.fromURL(imageUrl, (image: any) => {
      image.setAttrs({
        x: 0,
        y: 0,
        scaleX: 1,
        scaleY: 1,
      });
      // image.zIndex(1);
      layer.add(image);
      group.add(image);
      image.zIndex(0);
      layer.batchDraw();
    });
  }

  drawAreas(layer: Konva.Layer, group: Konva.Group, index: number): void {
    if (this.ctx) {
      for (let i = 0; i < this.data[index].data.length; i++) {
        const newGroup: Konva.Group = new Konva.Group();
        const datum = this.data[index].data[i];
        // const selectionAreaClass: SelectionAreaClass = new SelectionAreaClass(this.ctx);
        const box = new Konva.Rect({
          x: datum.left,
          y: datum.top,
          width: datum.right - datum.left,
          height: datum.bottom - datum.top,
          stroke: 'gray',
          fill: 'rgb(208,208,208)',
          opacity: 0.2,
          strokeWidth: 2,
          draggable: false,
          dash: [9, 3],
          dashEnabled: true,
        });

        const text = new Konva.Text({
          x: datum.left,
          y: datum.top,
          text: datum.text,
          opacity: 0,
          fontSize: 16,
          fontFamily: 'Calibri',
          fill: 'green',
        });

        newGroup.on('mousemove', (data: any) => {
          data.target.parent.children.map((child: Shape) => child.opacity(1));
          layer.draw();
        });

        newGroup.on('mouseout', (data: any) => {
          // TODO: add opacity 0 for text
          data.target.parent.children.map((child: Shape) => child.opacity(0.2));
          layer.draw();
        });

        newGroup.on('dblclick', () => {
          // hide text node and transformer:
          text.hide();
          layer.draw();

          // create textarea over canvas with absolute position
          // first we need to find position for textarea
          // how to find it?

          // at first lets find position of text node relative to the stage:
          const textPosition = text.absolutePosition();

          // then lets find position of stage container on the page:
          if (!this.ctx) {
            return;
          }
          const stageBox = this.ctx.container().getBoundingClientRect();

          // so position of textarea will be the sum of positions above:
          const areaPosition = {
            x: stageBox.left + textPosition.x,
            y: stageBox.top + textPosition.y,
          };

          // create textarea and style it
          const textarea = document.createElement('textarea');
          document.body.appendChild(textarea);

          // apply many styles to match text on canvas as close as possible
          // remember that text rendering on canvas and on the textarea can be different
          // and sometimes it is hard to make it 100% the same. But we will try...
          textarea.value = text.text();
          textarea.style.position = 'absolute';
          textarea.style.top = areaPosition.y + 'px';
          textarea.style.left = areaPosition.x + 'px';
          textarea.style.width = box.width() + 'px';
          textarea.style.height = box.height() + 'px';
          textarea.style.fontSize = text.fontSize() + 'px';
          textarea.style.border = 'none';
          textarea.style.padding = '0px';
          textarea.style.margin = '0px';
          textarea.style.overflow = 'hidden';
          textarea.style.background = 'none';
          textarea.style.outline = 'none';
          textarea.style.resize = 'none';
          textarea.style.lineHeight = text.lineHeight().toString();
          textarea.style.fontFamily = text.fontFamily();
          textarea.style.transformOrigin = 'left top';
          textarea.style.textAlign = text.align();
          textarea.style.color = text.fill();
          const transform = '';

          textarea.style.transform = transform;
          // reset height
          textarea.style.height = 'auto';
          // after browsers resized it we can set actual value
          textarea.style.height = textarea.scrollHeight + 3 + 'px';
          textarea.focus();

          /* tslint:disable */
          const handleOutsideClick = (e: any) => {
            if (e.target !== textarea) {
              text.text(textarea.value);
              removeTextarea();
            }
          };

          const removeTextarea = () => {
            if (textarea && textarea.parentNode) {
              textarea.parentNode.removeChild(textarea);
              window.removeEventListener('click', handleOutsideClick);
              text.show();
              layer.draw();
            }
          };
          /* tslint:enable */

          const setTextareaWidth = (newWidth: number) => {
            if (!newWidth) {
              // set width for placeholder
              newWidth = (text as any).placeholder.length * text.fontSize();
            }
            // some extra fixes on different browsers
            const isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
            const isFirefox = navigator.userAgent.toLowerCase().includes('firefox') === true;
            if (isSafari || isFirefox) {
              newWidth = Math.ceil(newWidth);
            }
            textarea.style.width = newWidth + 'px';
          };

          textarea.addEventListener('keydown', e => {
            // hide on enter
            // but don't hide on shift + enter
            if (e.keyCode === 13 && !e.shiftKey) {
              text.text(textarea.value);
              removeTextarea();
            }
            // on esc do not set value back to node
            if (e.keyCode === 27) {
              removeTextarea();
            }
          });

          textarea.addEventListener('keydown', () => {
            const scale = text.getAbsoluteScale().x;
            setTextareaWidth(text.width() * scale);
            textarea.style.height = 'auto';
            textarea.style.height = textarea.scrollHeight + text.fontSize() + 'px';
          });

          setTimeout(() => {
            window.addEventListener('click', handleOutsideClick);
          });
        });

        layer.add(box);
        layer.add(text);
        box.zIndex(0);
        text.zIndex(0);
        newGroup.add(box);
        newGroup.add(text);
        group.add(newGroup);
      }

      layer.add(group);

      layer.draw();
    }
  }
}

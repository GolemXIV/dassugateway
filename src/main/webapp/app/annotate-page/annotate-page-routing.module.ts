import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from 'app/home/home.component';
import { AnnotatePageComponent } from 'app/annotate-page/annotate-page.component';
import { Authority } from 'app/shared/constants/authority.constants';

const routes: Routes = [
  {
    path: 'workPage/:documentId/:documentPage',
    component: AnnotatePageComponent,
    data: {
      authorities: [Authority.USER, Authority.ADMIN],
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AnnotatePageRoutingModule {}

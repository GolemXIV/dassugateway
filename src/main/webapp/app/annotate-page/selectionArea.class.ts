export class SelectionAreaClass {
  constructor(private ctx: CanvasRenderingContext2D) {}

  draw({ text, top, bottom, left, right }: { text: string; top: number; bottom: number; left: number; right: number }): any {
    this.ctx.setLineDash([6]);
    this.ctx.fillText(text, left, top + (bottom - top) / 2, right - left);
    this.ctx.strokeRect(left, top, right - left, bottom - top);
  }
}

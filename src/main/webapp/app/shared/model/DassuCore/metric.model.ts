import { IFileCategory } from 'app/shared/model/DassuCore/file-category.model';
import { IFileCategoryFieldValue } from 'app/shared/model/DassuCore/file-category-field-value.model';
import { IMetricType } from 'app/shared/model/DassuCore/metric-type.model';

export interface IMetric {
  id?: number;
  key?: string;
  value?: number;
  fileCategory?: IFileCategory;
  fileCategoryFieldValue?: IFileCategoryFieldValue;
  metricType?: IMetricType;
}

export class Metric implements IMetric {
  constructor(
    public id?: number,
    public key?: string,
    public value?: number,
    public fileCategory?: IFileCategory,
    public fileCategoryFieldValue?: IFileCategoryFieldValue,
    public metricType?: IMetricType
  ) {}
}

import { IAtomicFile } from 'app/shared/model/DassuCore/atomic-file.model';
import { ICompositFile } from 'app/shared/model/DassuCore/composit-file.model';
import { StorageType } from 'app/shared/model/enumerations/storage-type.model';

export interface IBinaryFile {
  id?: number;
  name?: string;
  version?: number;
  storageFilePath?: string;
  md5?: string;
  storageType?: StorageType;
  atomic?: IAtomicFile;
  composit?: ICompositFile;
}

export class BinaryFile implements IBinaryFile {
  constructor(
    public id?: number,
    public name?: string,
    public version?: number,
    public storageFilePath?: string,
    public md5?: string,
    public storageType?: StorageType,
    public atomic?: IAtomicFile,
    public composit?: ICompositFile
  ) {}
}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IFileCategoryField } from 'app/shared/model/DassuCore/file-category-field.model';
import { FileCategoryFieldService } from './file-category-field.service';
import { FileCategoryFieldDeleteDialogComponent } from './file-category-field-delete-dialog.component';

@Component({
  selector: 'jhi-file-category-field',
  templateUrl: './file-category-field.component.html',
})
export class FileCategoryFieldComponent implements OnInit, OnDestroy {
  fileCategoryFields?: IFileCategoryField[];
  eventSubscriber?: Subscription;

  constructor(
    protected fileCategoryFieldService: FileCategoryFieldService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.fileCategoryFieldService
      .query()
      .subscribe((res: HttpResponse<IFileCategoryField[]>) => (this.fileCategoryFields = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInFileCategoryFields();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IFileCategoryField): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInFileCategoryFields(): void {
    this.eventSubscriber = this.eventManager.subscribe('fileCategoryFieldListModification', () => this.loadAll());
  }

  delete(fileCategoryField: IFileCategoryField): void {
    const modalRef = this.modalService.open(FileCategoryFieldDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.fileCategoryField = fileCategoryField;
  }
}

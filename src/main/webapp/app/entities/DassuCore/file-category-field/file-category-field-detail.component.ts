import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IFileCategoryField } from 'app/shared/model/DassuCore/file-category-field.model';

@Component({
  selector: 'jhi-file-category-field-detail',
  templateUrl: './file-category-field-detail.component.html',
})
export class FileCategoryFieldDetailComponent implements OnInit {
  fileCategoryField: IFileCategoryField | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ fileCategoryField }) => (this.fileCategoryField = fileCategoryField));
  }

  previousState(): void {
    window.history.back();
  }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IFileCategoryField } from 'app/shared/model/DassuCore/file-category-field.model';

type EntityResponseType = HttpResponse<IFileCategoryField>;
type EntityArrayResponseType = HttpResponse<IFileCategoryField[]>;

@Injectable({ providedIn: 'root' })
export class FileCategoryFieldService {
  public resourceUrl = SERVER_API_URL + 'services/dassucore/api/file-category-fields';

  constructor(protected http: HttpClient) {}

  create(fileCategoryField: IFileCategoryField): Observable<EntityResponseType> {
    return this.http.post<IFileCategoryField>(this.resourceUrl, fileCategoryField, { observe: 'response' });
  }

  update(fileCategoryField: IFileCategoryField): Observable<EntityResponseType> {
    return this.http.put<IFileCategoryField>(this.resourceUrl, fileCategoryField, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IFileCategoryField>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IFileCategoryField[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}

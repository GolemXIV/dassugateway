import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IFileCategoryField, FileCategoryField } from 'app/shared/model/DassuCore/file-category-field.model';
import { FileCategoryFieldService } from './file-category-field.service';
import { FileCategoryFieldComponent } from './file-category-field.component';
import { FileCategoryFieldDetailComponent } from './file-category-field-detail.component';
import { FileCategoryFieldUpdateComponent } from './file-category-field-update.component';

@Injectable({ providedIn: 'root' })
export class FileCategoryFieldResolve implements Resolve<IFileCategoryField> {
  constructor(private service: FileCategoryFieldService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IFileCategoryField> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((fileCategoryField: HttpResponse<FileCategoryField>) => {
          if (fileCategoryField.body) {
            return of(fileCategoryField.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new FileCategoryField());
  }
}

export const fileCategoryFieldRoute: Routes = [
  {
    path: '',
    component: FileCategoryFieldComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'dassuGatewayApp.dassuCoreFileCategoryField.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: FileCategoryFieldDetailComponent,
    resolve: {
      fileCategoryField: FileCategoryFieldResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'dassuGatewayApp.dassuCoreFileCategoryField.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: FileCategoryFieldUpdateComponent,
    resolve: {
      fileCategoryField: FileCategoryFieldResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'dassuGatewayApp.dassuCoreFileCategoryField.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: FileCategoryFieldUpdateComponent,
    resolve: {
      fileCategoryField: FileCategoryFieldResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'dassuGatewayApp.dassuCoreFileCategoryField.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];

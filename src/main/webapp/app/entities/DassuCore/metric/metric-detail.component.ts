import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMetric } from 'app/shared/model/DassuCore/metric.model';

@Component({
  selector: 'jhi-metric-detail',
  templateUrl: './metric-detail.component.html',
})
export class MetricDetailComponent implements OnInit {
  metric: IMetric | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ metric }) => (this.metric = metric));
  }

  previousState(): void {
    window.history.back();
  }
}

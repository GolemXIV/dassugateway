import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IMetricType, MetricType } from 'app/shared/model/DassuCore/metric-type.model';
import { MetricTypeService } from './metric-type.service';
import { MetricTypeComponent } from './metric-type.component';
import { MetricTypeDetailComponent } from './metric-type-detail.component';
import { MetricTypeUpdateComponent } from './metric-type-update.component';

@Injectable({ providedIn: 'root' })
export class MetricTypeResolve implements Resolve<IMetricType> {
  constructor(private service: MetricTypeService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IMetricType> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((metricType: HttpResponse<MetricType>) => {
          if (metricType.body) {
            return of(metricType.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new MetricType());
  }
}

export const metricTypeRoute: Routes = [
  {
    path: '',
    component: MetricTypeComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'dassuGatewayApp.dassuCoreMetricType.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: MetricTypeDetailComponent,
    resolve: {
      metricType: MetricTypeResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'dassuGatewayApp.dassuCoreMetricType.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: MetricTypeUpdateComponent,
    resolve: {
      metricType: MetricTypeResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'dassuGatewayApp.dassuCoreMetricType.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: MetricTypeUpdateComponent,
    resolve: {
      metricType: MetricTypeResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'dassuGatewayApp.dassuCoreMetricType.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];

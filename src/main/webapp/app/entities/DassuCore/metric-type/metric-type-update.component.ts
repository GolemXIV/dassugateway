import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IMetricType, MetricType } from 'app/shared/model/DassuCore/metric-type.model';
import { MetricTypeService } from './metric-type.service';

@Component({
  selector: 'jhi-metric-type-update',
  templateUrl: './metric-type-update.component.html',
})
export class MetricTypeUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [],
  });

  constructor(protected metricTypeService: MetricTypeService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ metricType }) => {
      this.updateForm(metricType);
    });
  }

  updateForm(metricType: IMetricType): void {
    this.editForm.patchValue({
      id: metricType.id,
      name: metricType.name,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const metricType = this.createFromForm();
    if (metricType.id !== undefined) {
      this.subscribeToSaveResponse(this.metricTypeService.update(metricType));
    } else {
      this.subscribeToSaveResponse(this.metricTypeService.create(metricType));
    }
  }

  private createFromForm(): IMetricType {
    return {
      ...new MetricType(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMetricType>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}

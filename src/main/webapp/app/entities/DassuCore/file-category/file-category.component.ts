import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IFileCategory } from 'app/shared/model/DassuCore/file-category.model';
import { FileCategoryService } from './file-category.service';
import { FileCategoryDeleteDialogComponent } from './file-category-delete-dialog.component';

@Component({
  selector: 'jhi-file-category',
  templateUrl: './file-category.component.html',
})
export class FileCategoryComponent implements OnInit, OnDestroy {
  fileCategories?: IFileCategory[];
  eventSubscriber?: Subscription;

  constructor(
    protected fileCategoryService: FileCategoryService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.fileCategoryService.query().subscribe((res: HttpResponse<IFileCategory[]>) => (this.fileCategories = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInFileCategories();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IFileCategory): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInFileCategories(): void {
    this.eventSubscriber = this.eventManager.subscribe('fileCategoryListModification', () => this.loadAll());
  }

  delete(fileCategory: IFileCategory): void {
    const modalRef = this.modalService.open(FileCategoryDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.fileCategory = fileCategory;
  }
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IFileCategory } from 'app/shared/model/DassuCore/file-category.model';

@Component({
  selector: 'jhi-file-category-detail',
  templateUrl: './file-category-detail.component.html',
})
export class FileCategoryDetailComponent implements OnInit {
  fileCategory: IFileCategory | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ fileCategory }) => (this.fileCategory = fileCategory));
  }

  previousState(): void {
    window.history.back();
  }
}

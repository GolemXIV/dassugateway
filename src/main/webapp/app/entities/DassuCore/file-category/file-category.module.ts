import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DassuGatewaySharedModule } from 'app/shared/shared.module';
import { FileCategoryComponent } from './file-category.component';
import { FileCategoryDetailComponent } from './file-category-detail.component';
import { FileCategoryUpdateComponent } from './file-category-update.component';
import { FileCategoryDeleteDialogComponent } from './file-category-delete-dialog.component';
import { fileCategoryRoute } from './file-category.route';

@NgModule({
  imports: [DassuGatewaySharedModule, RouterModule.forChild(fileCategoryRoute)],
  declarations: [FileCategoryComponent, FileCategoryDetailComponent, FileCategoryUpdateComponent, FileCategoryDeleteDialogComponent],
  entryComponents: [FileCategoryDeleteDialogComponent],
})
export class DassuCoreFileCategoryModule {}

import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IFileCategory, FileCategory } from 'app/shared/model/DassuCore/file-category.model';
import { FileCategoryService } from './file-category.service';
import { FileCategoryComponent } from './file-category.component';
import { FileCategoryDetailComponent } from './file-category-detail.component';
import { FileCategoryUpdateComponent } from './file-category-update.component';

@Injectable({ providedIn: 'root' })
export class FileCategoryResolve implements Resolve<IFileCategory> {
  constructor(private service: FileCategoryService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IFileCategory> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((fileCategory: HttpResponse<FileCategory>) => {
          if (fileCategory.body) {
            return of(fileCategory.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new FileCategory());
  }
}

export const fileCategoryRoute: Routes = [
  {
    path: '',
    component: FileCategoryComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'dassuGatewayApp.dassuCoreFileCategory.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: FileCategoryDetailComponent,
    resolve: {
      fileCategory: FileCategoryResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'dassuGatewayApp.dassuCoreFileCategory.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: FileCategoryUpdateComponent,
    resolve: {
      fileCategory: FileCategoryResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'dassuGatewayApp.dassuCoreFileCategory.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: FileCategoryUpdateComponent,
    resolve: {
      fileCategory: FileCategoryResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'dassuGatewayApp.dassuCoreFileCategory.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];

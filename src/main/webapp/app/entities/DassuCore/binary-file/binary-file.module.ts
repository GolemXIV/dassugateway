import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DassuGatewaySharedModule } from 'app/shared/shared.module';
import { BinaryFileComponent } from './binary-file.component';
import { BinaryFileDetailComponent } from './binary-file-detail.component';
import { BinaryFileUpdateComponent } from './binary-file-update.component';
import { BinaryFileDeleteDialogComponent } from './binary-file-delete-dialog.component';
import { binaryFileRoute } from './binary-file.route';

@NgModule({
  imports: [DassuGatewaySharedModule, RouterModule.forChild(binaryFileRoute)],
  declarations: [BinaryFileComponent, BinaryFileDetailComponent, BinaryFileUpdateComponent, BinaryFileDeleteDialogComponent],
  entryComponents: [BinaryFileDeleteDialogComponent],
})
export class DassuCoreBinaryFileModule {}

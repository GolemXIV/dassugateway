import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBinaryFile } from 'app/shared/model/DassuCore/binary-file.model';

@Component({
  selector: 'jhi-binary-file-detail',
  templateUrl: './binary-file-detail.component.html',
})
export class BinaryFileDetailComponent implements OnInit {
  binaryFile: IBinaryFile | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ binaryFile }) => (this.binaryFile = binaryFile));
  }

  previousState(): void {
    window.history.back();
  }
}

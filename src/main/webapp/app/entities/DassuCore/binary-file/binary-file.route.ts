import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IBinaryFile, BinaryFile } from 'app/shared/model/DassuCore/binary-file.model';
import { BinaryFileService } from './binary-file.service';
import { BinaryFileComponent } from './binary-file.component';
import { BinaryFileDetailComponent } from './binary-file-detail.component';
import { BinaryFileUpdateComponent } from './binary-file-update.component';

@Injectable({ providedIn: 'root' })
export class BinaryFileResolve implements Resolve<IBinaryFile> {
  constructor(private service: BinaryFileService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IBinaryFile> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((binaryFile: HttpResponse<BinaryFile>) => {
          if (binaryFile.body) {
            return of(binaryFile.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new BinaryFile());
  }
}

export const binaryFileRoute: Routes = [
  {
    path: '',
    component: BinaryFileComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'dassuGatewayApp.dassuCoreBinaryFile.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: BinaryFileDetailComponent,
    resolve: {
      binaryFile: BinaryFileResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'dassuGatewayApp.dassuCoreBinaryFile.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: BinaryFileUpdateComponent,
    resolve: {
      binaryFile: BinaryFileResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'dassuGatewayApp.dassuCoreBinaryFile.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: BinaryFileUpdateComponent,
    resolve: {
      binaryFile: BinaryFileResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'dassuGatewayApp.dassuCoreBinaryFile.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];

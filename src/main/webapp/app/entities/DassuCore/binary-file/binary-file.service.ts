import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IBinaryFile } from 'app/shared/model/DassuCore/binary-file.model';

type EntityResponseType = HttpResponse<IBinaryFile>;
type EntityArrayResponseType = HttpResponse<IBinaryFile[]>;

@Injectable({ providedIn: 'root' })
export class BinaryFileService {
  public resourceUrl = SERVER_API_URL + 'services/dassucore/api/binary-files';

  constructor(protected http: HttpClient) {}

  create(binaryFile: IBinaryFile): Observable<EntityResponseType> {
    return this.http.post<IBinaryFile>(this.resourceUrl, binaryFile, { observe: 'response' });
  }

  update(binaryFile: IBinaryFile): Observable<EntityResponseType> {
    return this.http.put<IBinaryFile>(this.resourceUrl, binaryFile, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IBinaryFile>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IBinaryFile[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}

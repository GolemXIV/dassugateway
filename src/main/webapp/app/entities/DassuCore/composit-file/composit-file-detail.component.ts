import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICompositFile } from 'app/shared/model/DassuCore/composit-file.model';

@Component({
  selector: 'jhi-composit-file-detail',
  templateUrl: './composit-file-detail.component.html',
})
export class CompositFileDetailComponent implements OnInit {
  compositFile: ICompositFile | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ compositFile }) => (this.compositFile = compositFile));
  }

  previousState(): void {
    window.history.back();
  }
}

import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ICompositFile, CompositFile } from 'app/shared/model/DassuCore/composit-file.model';
import { CompositFileService } from './composit-file.service';
import { CompositFileComponent } from './composit-file.component';
import { CompositFileDetailComponent } from './composit-file-detail.component';
import { CompositFileUpdateComponent } from './composit-file-update.component';

@Injectable({ providedIn: 'root' })
export class CompositFileResolve implements Resolve<ICompositFile> {
  constructor(private service: CompositFileService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICompositFile> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((compositFile: HttpResponse<CompositFile>) => {
          if (compositFile.body) {
            return of(compositFile.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new CompositFile());
  }
}

export const compositFileRoute: Routes = [
  {
    path: '',
    component: CompositFileComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'dassuGatewayApp.dassuCoreCompositFile.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CompositFileDetailComponent,
    resolve: {
      compositFile: CompositFileResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'dassuGatewayApp.dassuCoreCompositFile.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CompositFileUpdateComponent,
    resolve: {
      compositFile: CompositFileResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'dassuGatewayApp.dassuCoreCompositFile.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CompositFileUpdateComponent,
    resolve: {
      compositFile: CompositFileResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'dassuGatewayApp.dassuCoreCompositFile.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];

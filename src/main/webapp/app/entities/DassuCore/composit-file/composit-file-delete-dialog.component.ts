import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICompositFile } from 'app/shared/model/DassuCore/composit-file.model';
import { CompositFileService } from './composit-file.service';

@Component({
  templateUrl: './composit-file-delete-dialog.component.html',
})
export class CompositFileDeleteDialogComponent {
  compositFile?: ICompositFile;

  constructor(
    protected compositFileService: CompositFileService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.compositFileService.delete(id).subscribe(() => {
      this.eventManager.broadcast('compositFileListModification');
      this.activeModal.close();
    });
  }
}

import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IAtomicFile, AtomicFile } from 'app/shared/model/DassuCore/atomic-file.model';
import { AtomicFileService } from './atomic-file.service';
import { IBinaryFile } from 'app/shared/model/DassuCore/binary-file.model';
import { BinaryFileService } from 'app/entities/DassuCore/binary-file/binary-file.service';
import { ICompositFile } from 'app/shared/model/DassuCore/composit-file.model';
import { CompositFileService } from 'app/entities/DassuCore/composit-file/composit-file.service';

type SelectableEntity = IBinaryFile | ICompositFile;

@Component({
  selector: 'jhi-atomic-file-update',
  templateUrl: './atomic-file-update.component.html',
})
export class AtomicFileUpdateComponent implements OnInit {
  isSaving = false;
  binaryfiles: IBinaryFile[] = [];
  compositfiles: ICompositFile[] = [];

  editForm = this.fb.group({
    id: [],
    annotatedData: [],
    annotatedText: [],
    binaryFile: [],
    parent: [],
  });

  constructor(
    protected atomicFileService: AtomicFileService,
    protected binaryFileService: BinaryFileService,
    protected compositFileService: CompositFileService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ atomicFile }) => {
      this.updateForm(atomicFile);

      this.binaryFileService
        .query({ filter: 'atomic-is-null' })
        .pipe(
          map((res: HttpResponse<IBinaryFile[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IBinaryFile[]) => {
          if (!atomicFile.binaryFile || !atomicFile.binaryFile.id) {
            this.binaryfiles = resBody;
          } else {
            this.binaryFileService
              .find(atomicFile.binaryFile.id)
              .pipe(
                map((subRes: HttpResponse<IBinaryFile>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IBinaryFile[]) => (this.binaryfiles = concatRes));
          }
        });

      this.compositFileService.query().subscribe((res: HttpResponse<ICompositFile[]>) => (this.compositfiles = res.body || []));
    });
  }

  updateForm(atomicFile: IAtomicFile): void {
    this.editForm.patchValue({
      id: atomicFile.id,
      annotatedData: atomicFile.annotatedData,
      annotatedText: atomicFile.annotatedText,
      binaryFile: atomicFile.binaryFile,
      parent: atomicFile.parent,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const atomicFile = this.createFromForm();
    if (atomicFile.id !== undefined) {
      this.subscribeToSaveResponse(this.atomicFileService.update(atomicFile));
    } else {
      this.subscribeToSaveResponse(this.atomicFileService.create(atomicFile));
    }
  }

  private createFromForm(): IAtomicFile {
    return {
      ...new AtomicFile(),
      id: this.editForm.get(['id'])!.value,
      annotatedData: this.editForm.get(['annotatedData'])!.value,
      annotatedText: this.editForm.get(['annotatedText'])!.value,
      binaryFile: this.editForm.get(['binaryFile'])!.value,
      parent: this.editForm.get(['parent'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAtomicFile>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
